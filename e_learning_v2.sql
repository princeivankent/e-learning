/*
SQLyog Community v13.1.1 (64 bit)
MySQL - 5.7.21 : Database - e_learning_v2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`e_learning_v2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `e_learning_v2`;

/*Table structure for table `choices` */

DROP TABLE IF EXISTS `choices`;

CREATE TABLE `choices` (
  `choice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `choice_letter` enum('a','b','c','d') COLLATE utf8_unicode_ci NOT NULL,
  `choice` text COLLATE utf8_unicode_ci NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`choice_id`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `question_id` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `choices` */

insert  into `choices`(`choice_id`,`question_id`,`choice_letter`,`choice`,`is_correct`,`is_active`,`created_at`,`updated_at`) values 
(1,1,'a','Automobile Engines',1,1,'2018-09-19 16:30:57','2018-09-24 16:02:23'),
(2,1,'b','Automobile Suspension',0,1,'2018-09-19 16:30:57','2018-09-24 16:02:23'),
(3,1,'c','Automobile Transmission',0,1,'2018-09-19 16:30:57','2018-09-19 16:30:57'),
(4,1,'d','None of the above',0,1,'2018-09-19 16:30:57','2018-09-19 16:30:57'),
(5,2,'a','Gasoline',0,1,'2018-09-19 16:32:21','2018-09-19 16:32:34'),
(6,2,'b','Gas-Turbine Engine',0,1,'2018-09-19 16:32:21','2018-09-19 16:32:21'),
(7,2,'c','Diesel Engines',1,1,'2018-09-19 16:32:21','2018-09-19 16:32:34'),
(8,2,'d','Hybrid Engines',0,1,'2018-09-19 16:32:21','2018-09-19 16:32:21'),
(9,3,'a','Only is drawn in',0,1,'2018-09-19 16:34:11','2018-09-19 16:34:11'),
(10,3,'b','Both air and fuel is drawn in',1,1,'2018-09-19 16:34:11','2018-09-19 16:34:11'),
(11,3,'c','Only fuel is drawn in',0,1,'2018-09-19 16:34:11','2018-09-19 16:34:11'),
(12,3,'d','None of the above',0,1,'2018-09-19 16:34:11','2018-09-19 16:34:11'),
(13,4,'a','100%',0,1,'2018-09-19 16:34:58','2018-09-19 16:34:58'),
(14,4,'b','46%',1,1,'2018-09-19 16:34:58','2018-09-19 16:34:58'),
(15,4,'c','32%',0,1,'2018-09-19 16:34:58','2018-09-19 16:34:58'),
(16,4,'d','None of the above',0,1,'2018-09-19 16:34:58','2018-09-19 16:34:58'),
(17,5,'a','Intake Stroke',1,1,'2018-09-19 16:36:10','2018-09-19 16:36:10'),
(18,5,'b','Compression Stroke',0,1,'2018-09-19 16:36:10','2018-09-19 16:36:10'),
(19,5,'c','Combustion Stroke',0,1,'2018-09-19 16:36:10','2018-09-19 16:36:10'),
(20,5,'d','Exhaust',0,1,'2018-09-19 16:36:10','2018-09-19 16:36:10'),
(21,6,'a','Compression Ratio',0,1,'2018-09-19 16:37:33','2018-09-19 16:37:33'),
(22,6,'b','Air Fuel Ratio',0,1,'2018-09-19 16:37:33','2018-09-19 16:37:33'),
(23,6,'c','Pistion Displacement',1,1,'2018-09-19 16:37:33','2018-09-19 16:37:33'),
(24,6,'d','None of the above',0,1,'2018-09-19 16:37:33','2018-09-19 16:37:33'),
(25,7,'a','Stress-Strain Curve',0,1,'2018-09-19 16:39:15','2018-09-19 16:39:15'),
(26,7,'b','Engine Performance Curve',1,1,'2018-09-19 16:39:15','2018-09-19 16:39:15'),
(27,7,'c','Power Chart',0,1,'2018-09-19 16:39:15','2018-09-19 16:39:15'),
(28,7,'d','None of the above',0,1,'2018-09-19 16:39:15','2018-09-19 16:39:15'),
(29,8,'a','Power',0,1,'2018-09-19 16:39:56','2018-09-19 16:40:25'),
(30,8,'b','Torque',1,1,'2018-09-19 16:39:56','2018-09-19 16:40:25'),
(31,8,'c','Acceleration',0,1,'2018-09-19 16:39:56','2018-09-19 16:39:56'),
(32,8,'d','None',0,1,'2018-09-19 16:39:56','2018-09-19 16:39:56'),
(33,9,'a','Compression Ratio',1,1,'2018-09-19 16:43:13','2018-09-19 16:43:13'),
(34,9,'b','Air Fuel Ratio',0,1,'2018-09-19 16:43:13','2018-09-19 16:43:13'),
(35,9,'c','Piston Displacement',0,1,'2018-09-19 16:43:13','2018-09-19 16:43:13'),
(36,9,'d','None of the above',0,1,'2018-09-19 16:43:13','2018-09-19 16:43:13'),
(37,10,'a','Power',1,1,'2018-09-19 16:44:03','2018-09-19 16:44:03'),
(38,10,'b','Torque',0,1,'2018-09-19 16:44:03','2018-09-19 16:44:03'),
(39,10,'c','Acceleration',0,1,'2018-09-19 16:44:03','2018-09-19 16:44:03'),
(40,10,'d','None of the above',0,1,'2018-09-19 16:44:03','2018-09-19 16:44:03'),
(41,11,'a','1.495 L',0,1,'2018-09-19 16:51:44','2018-09-19 16:51:44'),
(42,11,'b','14.7 L',0,1,'2018-09-19 16:51:44','2018-09-19 16:51:44'),
(43,11,'c','2.999 L',1,1,'2018-09-19 16:51:44','2018-09-19 16:51:44'),
(44,11,'d','None of the above',0,1,'2018-09-19 16:51:44','2018-09-19 16:51:44'),
(45,12,'a','10 N-m',0,1,'2018-09-19 16:53:17','2018-09-19 16:53:17'),
(46,12,'b','6 N-m',1,1,'2018-09-19 16:53:17','2018-09-19 16:53:17'),
(47,12,'c','5.5 N-m',0,1,'2018-09-19 16:53:17','2018-09-19 16:53:17'),
(48,12,'d','None of the above',0,1,'2018-09-19 16:53:17','2018-09-19 16:53:17'),
(49,13,'a','4.199 L',0,1,'2018-09-19 17:00:31','2018-09-19 17:00:31'),
(50,13,'b','7.790 L',0,1,'2018-09-19 17:00:31','2018-09-19 17:00:31'),
(51,13,'c','2.489 L',1,1,'2018-09-19 17:00:31','2018-09-19 17:00:31'),
(52,13,'d','None of the above',0,1,'2018-09-19 17:00:31','2018-09-19 17:00:31'),
(53,14,'a','Cylinder Bore',0,1,'2018-09-19 17:01:39','2018-09-19 17:01:39'),
(54,14,'b','cylinder Stroke',0,1,'2018-09-19 17:01:39','2018-09-19 17:01:39'),
(55,14,'c','Cylinder Block',0,1,'2018-09-19 17:01:39','2018-09-19 17:01:39'),
(56,14,'d','Cylinder Head',1,1,'2018-09-19 17:01:39','2018-09-19 17:01:39'),
(57,15,'a','Camshaft',0,1,'2018-09-19 17:02:51','2018-09-19 17:02:51'),
(58,15,'b','Crankshaft',1,1,'2018-09-19 17:02:51','2018-09-19 17:02:51'),
(59,15,'c','Propeller Shaft',0,1,'2018-09-19 17:02:51','2018-09-19 17:02:51'),
(60,15,'d','None of the above',0,1,'2018-09-19 17:02:51','2018-09-19 17:02:51'),
(61,16,'a','Cylinder Bore',0,1,'2018-09-19 17:04:18','2018-09-19 17:04:18'),
(62,16,'b','Compression Stroke',0,1,'2018-09-19 17:04:18','2018-09-19 17:04:18'),
(63,16,'c','Cylinder Block',1,1,'2018-09-19 17:04:18','2018-09-19 17:04:18'),
(64,16,'d','Cylinder Head',0,1,'2018-09-19 17:04:18','2018-09-19 17:04:18'),
(65,17,'a','Integrated Cylinders',1,1,'2018-09-19 17:05:10','2018-09-19 17:05:10'),
(66,17,'b','Liner Type',0,1,'2018-09-19 17:05:10','2018-09-19 17:05:10'),
(67,17,'c','Open Liner Type',0,1,'2018-09-19 17:05:10','2018-09-19 17:05:10'),
(68,17,'d','None of the above',0,1,'2018-09-19 17:05:10','2018-09-19 17:05:10'),
(69,18,'a','Single Overhead Cam',0,1,'2018-09-19 17:06:30','2018-09-19 17:06:44'),
(70,18,'b','Double Overhead Cam',0,1,'2018-09-19 17:06:30','2018-09-19 17:06:30'),
(71,18,'c','Overhead Cam',0,1,'2018-09-19 17:06:30','2018-09-19 17:06:30'),
(72,18,'d','Overhead Valve',1,1,'2018-09-19 17:06:30','2018-09-19 17:06:44'),
(73,19,'a','Piston',0,1,'2018-09-19 17:07:49','2018-09-19 17:07:49'),
(74,19,'b','Valves',1,1,'2018-09-19 17:07:49','2018-09-19 17:07:49'),
(75,19,'c','Crankshaft',0,1,'2018-09-19 17:07:49','2018-09-19 17:07:49'),
(76,19,'d','None of the above',0,1,'2018-09-19 17:07:49','2018-09-19 17:07:49'),
(77,20,'a','Cooling System',1,1,'2018-09-19 17:08:49','2018-09-19 17:08:49'),
(78,20,'b','Fuel System',0,1,'2018-09-19 17:08:49','2018-09-19 17:08:49'),
(79,20,'c','Lubrication System',0,1,'2018-09-19 17:08:49','2018-09-19 17:08:49'),
(80,20,'d','Intake and Exhaust',0,1,'2018-09-19 17:08:49','2018-09-19 17:08:49');

/*Table structure for table `dealers` */

DROP TABLE IF EXISTS `dealers`;

CREATE TABLE `dealers` (
  `dealer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dealer_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `branch` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `is_authorized` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dealer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `dealers` */

insert  into `dealers`(`dealer_id`,`dealer_name`,`branch`,`is_authorized`,`is_active`,`created_at`,`updated_at`) values 
(8,'Isuzu GenCars Inc.','Makati',0,1,'2018-10-25 13:18:48','2018-10-25 13:18:48'),
(9,'Isuzu GenCars Inc.','Alabang',0,1,'2018-12-06 14:33:28','2018-12-06 14:33:28'),
(10,'Isuzu GenCars Inc.','Batangas',0,1,'2018-12-06 14:33:37','2018-12-06 14:33:37');

/*Table structure for table `emails` */

DROP TABLE IF EXISTS `emails`;

CREATE TABLE `emails` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_category` enum('creation','before','during','after','opened','finish','others') COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `recipient` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `cc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8_unicode_ci,
  `sent_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_id`),
  KEY `email_category_id` (`email_category`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `emails` */

insert  into `emails`(`email_id`,`email_category`,`subject`,`sender`,`recipient`,`title`,`message`,`cc`,`attachment`,`sent_at`,`created_at`,`updated_at`) values 
(1,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 10:49:58','2018-12-05 10:49:58'),
(2,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-05 10:50:31','2018-12-05 10:50:31'),
(3,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-05 10:50:31','2018-12-05 10:50:31'),
(4,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-05 10:50:31','2018-12-05 10:50:31'),
(5,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 10:52:11','2018-12-05 10:52:11'),
(6,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 10:52:11','2018-12-05 10:52:11'),
(7,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 10:52:11','2018-12-05 10:52:11'),
(8,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 14:30:48','2018-12-05 14:30:48'),
(9,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-05 16:02:54','2018-12-05 16:02:54'),
(10,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-05 16:02:54','2018-12-05 16:02:54'),
(11,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-05 16:02:54','2018-12-05 16:02:54'),
(12,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 16:09:08','2018-12-05 16:09:08'),
(13,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 16:09:08','2018-12-05 16:09:08'),
(14,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-05 16:09:08','2018-12-05 16:09:08'),
(15,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 07:53:03','2018-12-06 07:53:03'),
(16,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 07:53:14','2018-12-06 07:53:14'),
(17,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 07:53:14','2018-12-06 07:53:14'),
(18,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 07:53:14','2018-12-06 07:53:14'),
(19,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 08:40:46','2018-12-06 08:40:46'),
(20,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 08:40:46','2018-12-06 08:40:46'),
(21,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 08:44:00','2018-12-06 08:44:00'),
(22,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 08:44:00','2018-12-06 08:44:00'),
(23,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 08:44:00','2018-12-06 08:44:00'),
(24,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 08:49:44','2018-12-06 08:49:44'),
(25,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 08:49:44','2018-12-06 08:49:44'),
(26,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 08:49:44','2018-12-06 08:49:44'),
(27,'creation','New Module Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 09:53:51','2018-12-06 09:53:51'),
(28,'creation','New Module Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 09:53:51','2018-12-06 09:53:51'),
(29,'creation','New Module Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 09:55:24','2018-12-06 09:55:24'),
(30,'creation','New Module Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 09:55:24','2018-12-06 09:55:24'),
(31,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 09:57:22','2018-12-06 09:57:22'),
(32,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 09:57:22','2018-12-06 09:57:22'),
(33,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 09:57:22','2018-12-06 09:57:22'),
(34,'creation','New Module Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 10:00:11','2018-12-06 10:00:11'),
(35,'creation','New Module Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 10:00:11','2018-12-06 10:00:11'),
(36,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:05:44','2018-12-06 10:05:44'),
(37,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:05:44','2018-12-06 10:05:44'),
(38,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:05:44','2018-12-06 10:05:44'),
(39,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:05:59','2018-12-06 10:05:59'),
(40,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:05:59','2018-12-06 10:05:59'),
(41,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:05:59','2018-12-06 10:05:59'),
(42,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:15:01','2018-12-06 10:15:01'),
(43,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:15:01','2018-12-06 10:15:01'),
(44,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 10:15:01','2018-12-06 10:15:01'),
(45,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 11:01:14','2018-12-06 11:01:14'),
(46,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 11:01:14','2018-12-06 11:01:14'),
(47,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 11:01:14','2018-12-06 11:01:14'),
(48,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 11:45:34','2018-12-06 11:45:34'),
(49,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 11:45:34','2018-12-06 11:45:34'),
(50,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 11:45:34','2018-12-06 11:45:34'),
(51,'creation','New Module Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:01:33','2018-12-06 13:01:33'),
(52,'creation','New Module Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:01:33','2018-12-06 13:01:33'),
(53,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:02:21','2018-12-06 13:02:21'),
(54,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:02:21','2018-12-06 13:02:21'),
(55,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:02:21','2018-12-06 13:02:21'),
(56,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:05:35','2018-12-06 13:05:35'),
(57,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:05:35','2018-12-06 13:05:35'),
(58,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:05:35','2018-12-06 13:05:35'),
(59,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:06:17','2018-12-06 13:06:17'),
(60,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:06:17','2018-12-06 13:06:17'),
(61,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:43:24','2018-12-06 13:43:24'),
(62,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:43:24','2018-12-06 13:43:24'),
(63,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:43:24','2018-12-06 13:43:24'),
(64,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:45:33','2018-12-06 13:45:33'),
(65,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:45:33','2018-12-06 13:45:33'),
(66,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 13:45:33','2018-12-06 13:45:33'),
(67,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:49:41','2018-12-06 13:49:41'),
(68,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:49:41','2018-12-06 13:49:41'),
(69,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 13:49:41','2018-12-06 13:49:41'),
(70,'creation','New Module Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:34:12','2018-12-06 14:34:12'),
(71,'creation','New Module Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:34:12','2018-12-06 14:34:12'),
(72,'creation','New Module Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:36:35','2018-12-06 14:36:35'),
(73,'creation','New Module Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:36:35','2018-12-06 14:36:35'),
(74,'creation','New Module Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:36:35','2018-12-06 14:36:35'),
(75,'creation','New Module Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Module Schedule!</span>','Hi Mam/Sir, <strong>IPC Administration</strong> has been created a new schedule for a module viewing. <br>\r\n                    Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:36:35','2018-12-06 14:36:35'),
(76,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:37:43','2018-12-06 14:37:43'),
(77,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:37:43','2018-12-06 14:37:43'),
(78,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:37:43','2018-12-06 14:37:43'),
(79,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:53:35','2018-12-06 14:53:35'),
(80,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:53:35','2018-12-06 14:53:35'),
(81,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:53:35','2018-12-06 14:53:35'),
(82,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:14','2018-12-06 14:54:14'),
(83,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:14','2018-12-06 14:54:14'),
(84,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:14','2018-12-06 14:54:14'),
(85,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:26','2018-12-06 14:54:26'),
(86,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:26','2018-12-06 14:54:26'),
(87,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:26','2018-12-06 14:54:26'),
(88,'opened','Initialized Module','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:49','2018-12-06 14:54:49'),
(89,'opened','Initialized Module','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:49','2018-12-06 14:54:49'),
(90,'opened','Initialized Module','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the module viewing. <br>\r\n                            Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:49','2018-12-06 14:54:49'),
(91,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:59','2018-12-06 14:54:59'),
(92,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:59','2018-12-06 14:54:59'),
(93,'finish','Finished Module/PDF Viewing','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Module/PDF <span style=\"color: #5caad2;\">Finished!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n                    Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 14:54:59','2018-12-06 14:54:59'),
(94,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:58:48','2018-12-06 14:58:48'),
(95,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:58:48','2018-12-06 14:58:48'),
(96,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:58:48','2018-12-06 14:58:48'),
(97,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-06 14:58:48','2018-12-06 14:58:48'),
(98,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:04:09','2018-12-06 15:04:09'),
(99,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:04:09','2018-12-06 15:04:09'),
(100,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:04:09','2018-12-06 15:04:09'),
(101,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:15:26','2018-12-06 15:15:26'),
(102,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:15:26','2018-12-06 15:15:26'),
(103,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:15:26','2018-12-06 15:15:26'),
(104,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:19:55','2018-12-06 15:19:55'),
(105,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:19:55','2018-12-06 15:19:55'),
(106,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-06 15:19:55','2018-12-06 15:19:55'),
(107,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 09:50:17','2018-12-07 09:50:17'),
(108,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 09:50:17','2018-12-07 09:50:17'),
(109,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 09:50:17','2018-12-07 09:50:17'),
(110,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 09:50:17','2018-12-07 09:50:17'),
(111,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 09:51:37','2018-12-07 09:51:37'),
(112,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 09:51:37','2018-12-07 09:51:37'),
(113,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 09:51:37','2018-12-07 09:51:37'),
(114,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 10:43:24','2018-12-07 10:43:24'),
(115,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 10:43:24','2018-12-07 10:43:24'),
(116,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 10:43:24','2018-12-07 10:43:24'),
(117,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 10:43:24','2018-12-07 10:43:24'),
(118,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 11:30:47','2018-12-07 11:30:47'),
(119,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 11:30:47','2018-12-07 11:30:47'),
(120,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 11:30:47','2018-12-07 11:30:47'),
(121,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 11:30:47','2018-12-07 11:30:47'),
(122,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 12:50:08','2018-12-07 12:50:08'),
(123,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 12:50:08','2018-12-07 12:50:08'),
(124,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 12:50:08','2018-12-07 12:50:08'),
(125,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:20:58','2018-12-07 14:20:58'),
(126,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:20:58','2018-12-07 14:20:58'),
(127,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:20:58','2018-12-07 14:20:58'),
(128,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:20:58','2018-12-07 14:20:58'),
(129,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 14:23:44','2018-12-07 14:23:44'),
(130,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 14:23:44','2018-12-07 14:23:44'),
(131,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 14:23:44','2018-12-07 14:23:44'),
(132,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:26:20','2018-12-07 14:26:20'),
(133,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:26:20','2018-12-07 14:26:20'),
(134,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:26:20','2018-12-07 14:26:20'),
(135,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:59:22','2018-12-07 14:59:22'),
(136,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:59:22','2018-12-07 14:59:22'),
(137,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:59:22','2018-12-07 14:59:22'),
(138,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 14:59:22','2018-12-07 14:59:22'),
(139,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 14:59:45','2018-12-07 14:59:45'),
(140,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 14:59:45','2018-12-07 14:59:45'),
(141,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-07 14:59:45','2018-12-07 14:59:45'),
(142,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 15:01:41','2018-12-07 15:01:41'),
(143,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 15:01:41','2018-12-07 15:01:41'),
(144,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-07 15:01:41','2018-12-07 15:01:41'),
(145,'creation','New Exam Schedule','interface-notif@isuzuphil.com','prince-tiburcio@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 08:23:42','2018-12-10 08:23:42'),
(146,'creation','New Exam Schedule','interface-notif@isuzuphil.com','charisse@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 08:23:42','2018-12-10 08:23:42'),
(147,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mika@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 08:23:42','2018-12-10 08:23:42'),
(148,'creation','New Exam Schedule','interface-notif@isuzuphil.com','mycah@gmail.com','You have new <span style=\"color: #5caad2;\">Exam Schedule!</span>','Good Day! <strong>IPC Administration</strong> has been created a new schedule for a examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 08:23:42','2018-12-10 08:23:42'),
(149,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 09:13:45','2018-12-10 09:13:45'),
(150,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 09:13:45','2018-12-10 09:13:45'),
(151,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Makati</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 09:13:45','2018-12-10 09:13:45'),
(152,'finish','Finished Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 09:14:33','2018-12-10 09:14:33'),
(153,'finish','Finished Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 09:14:33','2018-12-10 09:14:33'),
(154,'finish','Finished Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Done!</span>','All Trainess of <strong>Isuzu GenCars Inc. | Makati</strong> has been successfully finished the examination. <br>\r\n						Please click the button to navigate directly to your system.',NULL,NULL,NULL,'2018-12-10 09:14:33','2018-12-10 09:14:33'),
(155,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 10:01:19','2018-12-10 10:01:19'),
(156,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 10:01:19','2018-12-10 10:01:19'),
(157,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Alabang</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 10:01:19','2018-12-10 10:01:19'),
(158,'opened','Initialized Examination','interface-notif@isuzuphil.com','marilou-cabarles@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 11:40:56','2018-12-10 11:40:56'),
(159,'opened','Initialized Examination','interface-notif@isuzuphil.com','carl-lat@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 11:40:56','2018-12-10 11:40:56'),
(160,'opened','Initialized Examination','interface-notif@isuzuphil.com','prince-tiburcio@isuzuphil.com','Examination <span style=\"color: #5caad2;\">Initiated!</span>','Isuzu GenCars Inc. of <strong>Batangas</strong> has been initiated the examination. <br>\r\n					Please click the button to navigate directly to our system.',NULL,NULL,NULL,'2018-12-10 11:40:56','2018-12-10 11:40:56');

/*Table structure for table `exam_details` */

DROP TABLE IF EXISTS `exam_details`;

CREATE TABLE `exam_details` (
  `exam_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_schedule_id` int(10) unsigned NOT NULL,
  `dealer_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_opened` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `status` enum('waiting','on_progress','ended','') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`exam_detail_id`),
  KEY `ed.exam_schedule_id` (`exam_schedule_id`),
  KEY `ed.dealer_id` (`dealer_id`),
  CONSTRAINT `ed.dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`dealer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ed.exam_schedule_id` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `exam_details` */

insert  into `exam_details`(`exam_detail_id`,`exam_schedule_id`,`dealer_id`,`start_date`,`end_date`,`is_opened`,`is_enabled`,`status`,`created_at`,`updated_at`) values 
(45,40,8,'2018-12-10','2018-12-10',1,1,'on_progress','2018-12-10 08:23:42','2018-12-10 09:13:45'),
(46,40,9,'2018-12-10','2018-12-10',1,1,'on_progress','2018-12-10 08:23:42','2018-12-10 10:01:19'),
(47,40,10,'2018-12-10','2018-12-10',1,1,'on_progress','2018-12-10 08:23:42','2018-12-10 11:40:56');

/*Table structure for table `exam_schedules` */

DROP TABLE IF EXISTS `exam_schedules`;

CREATE TABLE `exam_schedules` (
  `exam_schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `status` enum('on_going','completed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'on_going',
  `timer` int(11) NOT NULL,
  `passing_score` int(10) NOT NULL,
  `created_by` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PK',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`exam_schedule_id`),
  KEY `es.module_id` (`module_id`),
  CONSTRAINT `es.module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `exam_schedules` */

insert  into `exam_schedules`(`exam_schedule_id`,`module_id`,`status`,`timer`,`passing_score`,`created_by`,`created_at`,`updated_at`) values 
(40,1,'completed',5,10,'Prince Ivan Kent Tiburcio','2018-12-10 08:23:42','2018-12-10 09:14:32');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2018_07_18_070251_create_choices_table',1),
(4,'2018_07_18_071153_create_questions_table',2),
(5,'2018_07_18_071512_create_sub_modules_table',2),
(6,'2018_07_18_071734_create_modules_table',2),
(7,'2018_09_11_035513_create_sessions_table',3);

/*Table structure for table `module_details` */

DROP TABLE IF EXISTS `module_details`;

CREATE TABLE `module_details` (
  `module_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_schedule_id` int(10) unsigned NOT NULL,
  `trainor_id` int(10) unsigned DEFAULT NULL,
  `dealer_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_opened` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_finished` tinyint(1) unsigned DEFAULT '0',
  `status` enum('waiting','on_progress','ended') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module_detail_id`),
  KEY `md.dealer_id` (`dealer_id`),
  KEY `md.module_schedule_id` (`module_schedule_id`),
  CONSTRAINT `md.dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`dealer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `md.module_schedule_id` FOREIGN KEY (`module_schedule_id`) REFERENCES `module_schedules` (`module_schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `module_details` */

/*Table structure for table `module_schedules` */

DROP TABLE IF EXISTS `module_schedules`;

CREATE TABLE `module_schedules` (
  `module_schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `status` enum('on_going','completed','') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'on_going',
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PK',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module_schedule_id`),
  KEY `ms.module_id` (`module_id`),
  CONSTRAINT `ms.module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `module_schedules` */

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `file_name` text COLLATE utf8_unicode_ci,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `modules` */

insert  into `modules`(`module_id`,`module`,`description`,`file_name`,`is_active`,`created_at`,`updated_at`) values 
(1,'Engine Fundamentals','Operating Principles of Internal Combustion','Module 2 Engine Fundamentals.pdf',1,'2018-09-19 07:25:00','2018-09-24 09:11:35'),
(2,'Chassis Fundamentals','Manual/Automatic Transmission','Module 3 Chassis Fundamentals.pdf',1,'2018-09-19 07:27:30','2018-09-19 07:27:30'),
(3,'Electrical Fundamentals','Theory of electricity, Power, Load and Ground','Module 4 Electrical Fundamentals.pdf',1,'2018-09-19 07:28:26','2018-09-19 07:28:26');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `question_details` */

DROP TABLE IF EXISTS `question_details`;

CREATE TABLE `question_details` (
  `question_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_schedule_id` int(10) unsigned NOT NULL,
  `sub_module_id` int(10) unsigned NOT NULL,
  `items` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`question_detail_id`),
  KEY `qd.sub_module_id` (`sub_module_id`),
  KEY `qd.exam_schedule_id` (`exam_schedule_id`),
  CONSTRAINT `qd.exam_schedule_id` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `qd.sub_module_id` FOREIGN KEY (`sub_module_id`) REFERENCES `sub_modules` (`sub_module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `question_details` */

insert  into `question_details`(`question_detail_id`,`exam_schedule_id`,`sub_module_id`,`items`,`created_at`,`updated_at`) values 
(67,40,1,5,'2018-12-10 08:23:42','2018-12-10 08:23:42'),
(68,40,2,5,'2018-12-10 08:23:42','2018-12-10 08:23:42');

/*Table structure for table `questions` */

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_module_id` int(10) unsigned NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`question_id`),
  KEY `sub_module_id` (`sub_module_id`),
  CONSTRAINT `sub_module_id` FOREIGN KEY (`sub_module_id`) REFERENCES `sub_modules` (`sub_module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `questions` */

insert  into `questions`(`question_id`,`sub_module_id`,`question`,`is_active`,`created_at`,`updated_at`) values 
(1,1,'A type of machine designed to produce power and to propel automobiles to be able to go to from point A to point B.',1,'2018-09-19 08:30:57','2018-09-19 08:30:57'),
(2,1,'A type of engine in which it uses light oil as fuel and is mostly used in commercial vehicles due to fuel economy and high thermal efficiency.',1,'2018-09-19 08:32:21','2018-09-19 08:32:21'),
(3,1,'It is one of the unique features of Gasoline Engines compared to Diesel Engines during intake stroke.',1,'2018-09-19 08:34:11','2018-09-19 08:34:11'),
(4,1,'The thermal efficiency of Diesel Engines as compared to Gasoline Engines.',1,'2018-09-19 08:34:58','2018-09-19 08:34:58'),
(5,1,'For Diesel Engines, it is the stroke in which only air enters the cylinders as the piston goes down to BDC.',1,'2018-09-19 08:36:10','2018-09-19 08:36:10'),
(6,1,'It is the volume discharged when the piston moves from bottom dead center to top dead center.',1,'2018-09-19 08:37:33','2018-09-19 08:37:33'),
(7,1,'It is a graph that shows the power and torque according to ranges of engine speed and indicates the characteristics of the engine depending on the RPM.',1,'2018-09-19 08:39:15','2018-09-19 08:39:15'),
(8,1,'It is the ability to cause something to rotate or turning force.',1,'2018-09-19 08:39:56','2018-09-19 08:39:56'),
(9,1,'It is the ratio of the volume above the piston when the piston is at BDC to the volume of the piston when it is at TDC.',1,'2018-09-19 08:43:13','2018-09-19 08:43:13'),
(10,1,'The work volume and speed required to do the work or work volume per unit of time is called',1,'2018-09-19 08:44:03','2018-09-19 08:44:03'),
(11,2,'Given the following data, compute for the engine displacement.\r\nBore = 95.4mm\r\nStroke = 104.9 mm\r\nNo. of Cylinders = 4',1,'2018-09-19 08:51:44','2018-09-19 08:51:44'),
(12,2,'How much torque was applied to a bolt when a 20N force was applied to a power handle 0.3 m long?',1,'2018-09-19 08:53:17','2018-09-19 08:53:17'),
(13,2,'Given the following data, compute for the engine displacement.\r\nBore = 115 mm\r\nStroke = 125 mm\r\nNo. of Cylinders = 6',1,'2018-09-19 09:00:31','2018-09-19 09:00:31'),
(14,2,'It sits above the cylinder block and closes the top of the cylinder forming the combustion chamber.',1,'2018-09-19 09:01:39','2018-09-19 09:01:39'),
(15,2,'It receives combustion pressure via piston and connecting rod, and converts the reciprocating motion of the piston to a rotary motion.',1,'2018-09-19 09:02:51','2018-09-19 09:02:51'),
(16,2,'It is the foundation of the engine in which all of the parts of the engine is attached to it and is usually made of cast iron for strength and durability particularly for diesel engines.',1,'2018-09-19 09:04:18','2018-09-19 09:04:18'),
(17,2,'It is one ot the types of cylinders in which the cylinder is machined in the cylinder block.',1,'2018-09-19 09:05:10','2018-09-19 09:05:10'),
(18,2,'A type of valve configuration in which the camshaft is located inside the block and valves move via push rods and rocker arms.',1,'2018-09-19 09:06:30','2018-09-19 09:06:30'),
(19,2,'It directs flow of intake and exhaust gases to and from the engine and are part of the combustion chamber.',1,'2018-09-19 09:07:49','2018-09-19 09:07:49'),
(20,2,'This engine system maintains control of the engine temperature to an optimum level.',1,'2018-09-19 09:08:49','2018-09-19 09:08:49');

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sessions` */

insert  into `sessions`(`id`,`user_id`,`ip_address`,`user_agent`,`payload`,`last_activity`) values 
('2cVwakzomDLYT2KgYCALmIB4jlPBFDiN5Eiwlows',44,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiZXc4c2U1SWZGRU5vR1NKZDZQYVo0eWFJQjB0cWFlU0pST2d2TXFtUiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzU6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy90cmFpbmVlIjt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NDQ7fQ==',1544413350),
('4JwuLBLDvCxd6ug6M8QB08kanmAQNLguXwAmJQO3',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MjoiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL3RyYWlub3JzIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6IkpUM240YVpoUjJQM21VSmtpTHl3Wndyb2FjVjV3ZGh1ZHdUcm81R08iO3M6MTE6ImVtcGxveWVlX2lkIjtzOjQ6IjEyNTciO3M6MTE6ImVtcGxveWVlX25vIjtzOjY6IjE4MDcwNCI7czo5OiJmdWxsX25hbWUiO3M6MjU6IlBSSU5DRSBJVkFOIEtFTlQgVElCVVJDSU8iO3M6Nzoic2VjdGlvbiI7czoyOiIxOCI7fQ==',1543823886),
('5mBJGt9Ocy2DwybVV8Ez8JQdPSvgrxainCtXBHwr',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MjoiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL3RyYWlub3JzIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6ImtRZWJ1c04xb0RkcEU0bXk5dDdIS0p0ZWNOVFE1aUZrMjJRczRPTTYiO3M6MTE6ImVtcGxveWVlX2lkIjtzOjQ6IjEyNTciO3M6MTE6ImVtcGxveWVlX25vIjtzOjY6IjE4MDcwNCI7czo5OiJmdWxsX25hbWUiO3M6MjU6IlBSSU5DRSBJVkFOIEtFTlQgVElCVVJDSU8iO3M6Nzoic2VjdGlvbiI7czoyOiIxOCI7fQ==',1544068910),
('68DUHGZAIiUzAEjKkbkwGexswtvBn6NivpGvj77m',34,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiSHdWbnIyS2d1SXFSdnI1YUhzang3YmxvbE1jT0xzUnNpenIwbllMQyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy90cmFpbmVlL2V4YW0vNDIiO31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aTozNDt9',1544170488),
('a4wTLQ1cH3Up6G4KsGDCreA7x3a9brIjXxL8hTd1',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoieHVxM0hyNzF3ZUl3OUl5WWw5cDE2ckl2aFdVQWZJaEJSU0xmUGt3ZCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1543823834),
('AoFaJBk6rRVH3TpkHdqhZU8YdqZe9U1rbT7yPk2Q',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YToyOntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MToiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2ZsdXNoX3Nlc3Npb24iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1543911350),
('AsvFHIochZJOQOK7CdjufLmX2pIOT1bhwmtOnzl8',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo2OToiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL3Jlc3VsdHMvZXhhbV9zY2hlZHVsZXMvMzkvZGVhbGVycy84Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6ImV6cjFYS0JYVHVzS1FKMGJHaDBnZEdXdlVSRkc5elFacVMzcTdpTjMiO3M6MTE6ImVtcGxveWVlX2lkIjtzOjQ6IjEyNTciO3M6MTE6ImVtcGxveWVlX25vIjtzOjY6IjE4MDcwNCI7czo5OiJmdWxsX25hbWUiO3M6MjU6IlBSSU5DRSBJVkFOIEtFTlQgVElCVVJDSU8iO3M6Nzoic2VjdGlvbiI7czoyOiIxOCI7fQ==',1544166554),
('B4pEvTqPSo2uLZapUQi88csJGMRcNNHa8J4O5P0B',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YToyOntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MToiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2ZsdXNoX3Nlc3Npb24iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1543997723),
('Dw8dtSg4ZIUwdN5jm52eAHUHEroj9CqGjsTSTeym',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YToyOntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MToiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2ZsdXNoX3Nlc3Npb24iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1543398181),
('DwkW6FmvoC7jAtpsEWjRkKsVeDvUOL4Uu3IQ20Ue',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo3OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiTlpmbGU5eWVTVWVIQlFGbGdBWlBhUnVvbVF5TmxuVVJLOEpHakd0cyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTY6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9hZG1pbi9yZXN1bHRzL2V4YW1fc2NoZWR1bGVzIjt9czoxMToiZW1wbG95ZWVfaWQiO3M6NDoiMTI1NyI7czoxMToiZW1wbG95ZWVfbm8iO3M6NjoiMTgwNzA0IjtzOjk6ImZ1bGxfbmFtZSI7czoyNToiUFJJTkNFIElWQU4gS0VOVCBUSUJVUkNJTyI7czo3OiJzZWN0aW9uIjtzOjI6IjE4Ijt9',1544414252),
('FjdxpWOoWvgQ9VcPlpB4vRIOXSCJyj2D2wk74mcc',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MjoiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL3RyYWluZWVzIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6InZuZnJiS1Q3bkRncGUwaVVJb0RYTlBhM3hkVFkzTGdyNXpqREllYlIiO3M6MTE6ImVtcGxveWVlX2lkIjtzOjQ6IjEyNTciO3M6MTE6ImVtcGxveWVlX25vIjtzOjY6IjE4MDcwNCI7czo5OiJmdWxsX25hbWUiO3M6MjU6IlBSSU5DRSBJVkFOIEtFTlQgVElCVVJDSU8iO3M6Nzoic2VjdGlvbiI7czoyOiIxOCI7fQ==',1544140688),
('FRLjYfnkmcmS7Gwo7cVmqs8yqwjCFifx9wiAITaI',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoidmlua2ZPcXZwMk1wNmowZFUzM0NzVEVnbDdlazNLTVdpV3p6cXM3NCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1544083923),
('gnQlnaDlZixogSfeyDiJwH3MPSHWrAzjQfUCx8DO',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiQ0NBNzRwMUFzbExQZXNPbHVBakJQMXYxQ2ZPbGhmaDZ6bTVyMlMyWiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1543994292),
('IHsfRpXlwtHWKyScPd9fKDRgh1uY6Ia5AGR6hZOq',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiTWhwSFNQMk5UbjN1RGR6bzRUZWVET3F2aWltTUhKOUxqaE55cjFsMCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1543799842),
('KB2QI0xTsjstp7ESnPmdNUOfHlrGOk82evs1Z0Ub',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo1MzoiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL2RlYWxlcl9zdW1tYXJ5LzgvMzQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiZEVsRDNtUXRwWWZKM1ZjRVJSSWZpRjE5UkJPTUtqODBjYzFBMUR4bCI7czoxMToiZW1wbG95ZWVfaWQiO3M6NDoiMTI1NyI7czoxMToiZW1wbG95ZWVfbm8iO3M6NjoiMTgwNzA0IjtzOjk6ImZ1bGxfbmFtZSI7czoyNToiUFJJTkNFIElWQU4gS0VOVCBUSUJVUkNJTyI7czo3OiJzZWN0aW9uIjtzOjI6IjE4Ijt9',1544161282),
('kXldw9suiOD6Ad7KWNrhKXSL11XP7kKILvVFSM3C',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo2MDoiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL3Jlc3VsdHMvZXhhbV9zY2hlZHVsZXMvZ2V0Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6IkltemI0d1lIOEIxNVR3b1Z0TVNvSDZDRkJmeGZpQnkwdzRxVHpidmIiO30=',1544084107),
('Lkr0gvA8rBtNw9RV2L3vKTNVXS1pLQFdiWIMSRyU',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiYjhKUmpoYXloTjAyMVhNbFlYSkFNN0R1OWVSNm5iMGdVbXdyNnh2RyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1543895265),
('LQbS7l0EwWPaO2h4vw2kMnduMxcAYv3NwrunKpZF',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiOEM3QmR6VXpHRmtVRmRYYzg5c1JBc2dCYnpucFdmcFlBMm52SVdYRyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1543478615),
('M336SZwoUFZ0V1HceRtRWDXEERgUFwzZ7v6To7RI',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiUm5waGlVTGlGUXlwV3dDTDNwN040VTZyNU1ON05KWXBxV1JXSU5oVyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1543997716),
('PJGvWHY0p4SXRcupXBCJlqkXLLfE14suNNiGBGcu',34,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTo0OntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiRUR3aWVlcnVydEx3TnZNbnYydm1rMlR2TDFtSzU4NW1Ja0tZcGlNbiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzU6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy90cmFpbmVlIjt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MzQ7fQ==',1543978331),
('POhItBww8jLN6VouAgHWBU6hcS8U5ZabQ4zefLqF',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo0MToiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL2RlYWxlcnMiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiNnZCNkNOS2FXYndiOW55WG10MVg1SEhjZnZtazJnaTd6bm5OdW5mSyI7czoxMToiZW1wbG95ZWVfaWQiO3M6NDoiMTI1NyI7czoxMToiZW1wbG95ZWVfbm8iO3M6NjoiMTgwNzA0IjtzOjk6ImZ1bGxfbmFtZSI7czoyNToiUFJJTkNFIElWQU4gS0VOVCBUSUJVUkNJTyI7czo3OiJzZWN0aW9uIjtzOjI6IjE4Ijt9',1543453008),
('Q8h6cOEBXQrsy8Hm8Pd0CNB0QBzhuI4EMwzceo6B',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTo3OntzOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czo4MToiaHR0cDovL2xvY2FsaG9zdC9lLWxlYXJuaW5nL2FkbWluL3Jlc3VsdHMvZXhhbV9zY2hlZHVsZXMvMTgvZGVhbGVycy84L3RyYWluZWVzLzEzIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo2OiJfdG9rZW4iO3M6NDA6IjZmMzZkRG5scUdFTTZGSG9QN2ZURWd2dVdpeGhYTTFoZEdFRW9rbVAiO3M6MTE6ImVtcGxveWVlX2lkIjtzOjQ6IjEyNTciO3M6MTE6ImVtcGxveWVlX25vIjtzOjY6IjE4MDcwNCI7czo5OiJmdWxsX25hbWUiO3M6MjU6IlBSSU5DRSBJVkFOIEtFTlQgVElCVVJDSU8iO3M6Nzoic2VjdGlvbiI7czoyOiIxOCI7fQ==',1543478868),
('qymHKNNjGKLTFBC1zpOgW4sLVIHL7XmKBv1SLEWX',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiMW9XQ2hZUElwRUxHcUp4U3c3RzRPNlREQkJCb3g3NTJVdkMyeWRBayI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9sb2dpbiI7fX0=',1544160030),
('RJ5icZD2FXZt8PZ8MI3PtxDWt7F0vbMyH8jXVjTv',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiZUhiZTNMNUN3a1pVUHdRRVhla0pabnV6ZW1GVWNhR1NZb0ZSbTVRViI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTM6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9hZG1pbi9kZWFsZXJfc3VtbWFyeS84LzM0Ijt9fQ==',1544400765),
('SHyoA0zjIodk19fnLbgaVIj48P8hvE3lqgScNda1',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiU0QyZzZhWHlDVHlibXplUnFnMU5OZVA4V0tJc24wUmZxMmptZWMwaCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDI6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9hZG1pbi90cmFpbm9ycyI7fX0=',1544080413),
('uT4YOcAC2wOIqM2QsEadHNqAEqzqKXGdiKmzhStm',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36','YTozOntzOjY6Il90b2tlbiI7czo0MDoiWFhnN09MNU8weE0zT2RIbzV4dTlvU0FoMnFlbmcxcmZaMmhiVXZ4cSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDI6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9hZG1pbi90cmFpbm9ycyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=',1544072264),
('W18rLsZI9NHWJbD3qN24YiJXl3gUYnrilpqTpJPS',NULL,'::1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36','YTozOntzOjY6Il90b2tlbiI7czo0MDoieExBdTJmOFAwczZuQXI0UnRtS1VEZ3IyeXZHUEk2cnFSekFOVmNFbiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Njc6Imh0dHA6Ly9sb2NhbGhvc3QvZS1sZWFybmluZy9hZG1pbi9yZXN1bHRzL2V4YW1fc2NoZWR1bGVzLzE0L2RlYWxlcnMiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19',1543389453);

/*Table structure for table `sub_modules` */

DROP TABLE IF EXISTS `sub_modules`;

CREATE TABLE `sub_modules` (
  `sub_module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `sub_module` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sub_module_id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sub_modules` */

insert  into `sub_modules`(`sub_module_id`,`module_id`,`sub_module`,`is_active`,`created_at`,`updated_at`) values 
(1,1,'Operating Principles of Internal Combustion Engines (Gasoline & Diesel)',1,'2018-09-19 08:01:49','2018-09-19 08:01:49'),
(2,1,'Engine Performance',1,'2018-09-19 08:06:29','2018-09-19 08:06:29'),
(3,1,'Engine Structure',1,'2018-09-19 08:06:37','2018-09-19 08:06:37'),
(4,1,'Engine Systems',1,'2018-09-19 08:06:42','2018-09-19 08:06:42'),
(5,1,'Engine Model Line-up for CV\'s and LCV\'s',1,'2018-09-19 08:07:01','2018-09-19 08:07:01'),
(6,2,'Transmission',1,'2018-09-19 08:14:22','2018-09-19 08:14:22'),
(7,2,'Differential and Final Drive',1,'2018-09-19 08:14:32','2018-09-19 08:14:32'),
(8,2,'Propeller Shaft',1,'2018-09-19 08:14:41','2018-09-19 08:14:41'),
(9,2,'Suspension System',1,'2018-09-19 08:14:47','2018-09-19 08:14:47'),
(10,2,'Wheel Alignment',1,'2018-09-19 08:14:59','2018-09-19 08:14:59'),
(11,2,'Brake System',1,'2018-09-19 08:15:07','2018-09-19 08:15:07'),
(12,2,'Steering System',1,'2018-09-19 08:24:54','2018-09-19 08:24:54'),
(13,2,'Wheels and Tires',1,'2018-09-19 08:25:01','2018-09-19 08:25:01'),
(14,3,'Introduction to Electricity',1,'2018-09-19 08:26:44','2018-09-19 08:26:44'),
(15,3,'Electrical Circuits',1,'2018-09-19 08:26:52','2018-09-19 08:26:52'),
(16,3,'Wiring Diagram',1,'2018-09-19 08:26:57','2018-09-19 08:26:57'),
(17,3,'Engine Electrical Systems',1,'2018-09-19 08:27:05','2018-09-19 08:27:05'),
(18,3,'Air Conditioning System',1,'2018-09-19 08:27:21','2018-09-19 08:27:21');

/*Table structure for table `trainee_exams` */

DROP TABLE IF EXISTS `trainee_exams`;

CREATE TABLE `trainee_exams` (
  `trainee_exam_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_schedule_id` int(10) unsigned NOT NULL,
  `trainee_id` int(10) unsigned NOT NULL,
  `remaining_time` int(10) NOT NULL,
  `seconds` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trainee_exam_id`),
  KEY `exam_schedule_id` (`exam_schedule_id`),
  KEY `trainee_id` (`trainee_id`),
  CONSTRAINT `exam_schedule_id` FOREIGN KEY (`exam_schedule_id`) REFERENCES `exam_schedules` (`exam_schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trainee_id` FOREIGN KEY (`trainee_id`) REFERENCES `trainees` (`trainee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `trainee_exams` */

insert  into `trainee_exams`(`trainee_exam_id`,`exam_schedule_id`,`trainee_id`,`remaining_time`,`seconds`,`created_at`,`updated_at`) values 
(53,40,15,0,0,'2018-12-10 09:10:25','2018-12-10 09:14:32'),
(54,40,16,0,0,'2018-12-10 09:21:30','2018-12-10 09:23:09'),
(55,40,19,0,0,'2018-12-10 09:35:33','2018-12-10 09:37:35'),
(56,40,17,0,0,'2018-12-10 10:01:19','2018-12-10 10:03:10'),
(57,40,18,0,0,'2018-12-10 11:40:56','2018-12-10 11:42:29');

/*Table structure for table `trainee_questions` */

DROP TABLE IF EXISTS `trainee_questions`;

CREATE TABLE `trainee_questions` (
  `trainee_question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trainee_exam_id` int(10) unsigned NOT NULL,
  `number` int(10) NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `choice_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trainee_question_id`),
  KEY `trainee_exam_id` (`trainee_exam_id`),
  KEY `tq.question_id` (`question_id`),
  CONSTRAINT `tq.question_id` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trainee_exam_id` FOREIGN KEY (`trainee_exam_id`) REFERENCES `trainee_exams` (`trainee_exam_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `trainee_questions` */

insert  into `trainee_questions`(`trainee_question_id`,`trainee_exam_id`,`number`,`question_id`,`choice_id`,`create_at`,`updated_at`) values 
(235,53,1,5,17,'2018-12-10 09:10:25','2018-12-10 09:13:56'),
(236,53,2,6,21,'2018-12-10 09:10:25','2018-12-10 09:14:03'),
(237,53,3,1,3,'2018-12-10 09:10:25','2018-12-10 09:14:05'),
(238,53,4,7,26,'2018-12-10 09:10:25','2018-12-10 09:14:07'),
(239,53,5,10,37,'2018-12-10 09:10:25','2018-12-10 09:14:10'),
(240,53,6,14,55,'2018-12-10 09:10:25','2018-12-10 09:14:11'),
(241,53,7,15,59,'2018-12-10 09:10:25','2018-12-10 09:14:24'),
(242,53,8,20,77,'2018-12-10 09:10:25','2018-12-10 09:14:25'),
(243,53,9,16,62,'2018-12-10 09:10:25','2018-12-10 09:14:27'),
(244,53,10,12,47,'2018-12-10 09:10:25','2018-12-10 09:14:29'),
(245,54,1,4,13,'2018-12-10 09:21:30','2018-12-10 09:22:19'),
(246,54,2,7,27,'2018-12-10 09:21:30','2018-12-10 09:22:27'),
(247,54,3,1,1,'2018-12-10 09:21:30','2018-12-10 09:22:32'),
(248,54,4,8,31,'2018-12-10 09:21:30','2018-12-10 09:22:38'),
(249,54,5,3,11,'2018-12-10 09:21:30','2018-12-10 09:22:42'),
(250,54,6,12,47,'2018-12-10 09:21:30','2018-12-10 09:22:44'),
(251,54,7,18,69,'2018-12-10 09:21:30','2018-12-10 09:22:47'),
(252,54,8,15,59,'2018-12-10 09:21:30','2018-12-10 09:22:54'),
(253,54,9,14,53,'2018-12-10 09:21:30','2018-12-10 09:23:01'),
(254,54,10,17,65,'2018-12-10 09:21:30','2018-12-10 09:23:04'),
(255,55,1,1,1,'2018-12-10 09:35:34','2018-12-10 09:35:47'),
(256,55,2,5,17,'2018-12-10 09:35:34','2018-12-10 09:35:55'),
(257,55,3,4,13,'2018-12-10 09:35:34','2018-12-10 09:36:00'),
(258,55,4,8,30,'2018-12-10 09:35:34','2018-12-10 09:36:06'),
(259,55,5,10,37,'2018-12-10 09:35:34','2018-12-10 09:36:15'),
(260,55,6,13,50,'2018-12-10 09:35:34','2018-12-10 09:36:31'),
(261,55,7,18,72,'2018-12-10 09:35:34','2018-12-10 09:36:56'),
(262,55,8,12,46,'2018-12-10 09:35:34','2018-12-10 09:37:07'),
(263,55,9,11,43,'2018-12-10 09:35:34','2018-12-10 09:37:22'),
(264,55,10,15,58,'2018-12-10 09:35:34','2018-12-10 09:37:33'),
(265,56,1,6,23,'2018-12-10 10:01:19','2018-12-10 10:01:30'),
(266,56,2,3,10,'2018-12-10 10:01:19','2018-12-10 10:01:39'),
(267,56,3,2,7,'2018-12-10 10:01:19','2018-12-10 10:01:48'),
(268,56,4,9,33,'2018-12-10 10:01:19','2018-12-10 10:01:56'),
(269,56,5,10,37,'2018-12-10 10:01:19','2018-12-10 10:02:19'),
(270,56,6,13,50,'2018-12-10 10:01:19','2018-12-10 10:02:22'),
(271,56,7,12,46,'2018-12-10 10:01:19','2018-12-10 10:02:43'),
(272,56,8,11,43,'2018-12-10 10:01:19','2018-12-10 10:02:47'),
(273,56,9,15,58,'2018-12-10 10:01:19','2018-12-10 10:02:55'),
(274,56,10,14,56,'2018-12-10 10:01:19','2018-12-10 10:03:07'),
(275,57,1,1,1,'2018-12-10 11:40:56','2018-12-10 11:41:11'),
(276,57,2,5,17,'2018-12-10 11:40:56','2018-12-10 11:41:17'),
(277,57,3,8,30,'2018-12-10 11:40:56','2018-12-10 11:41:23'),
(278,57,4,7,26,'2018-12-10 11:40:56','2018-12-10 11:41:29'),
(279,57,5,3,10,'2018-12-10 11:40:56','2018-12-10 11:41:43'),
(280,57,6,15,58,'2018-12-10 11:40:56','2018-12-10 11:41:45'),
(281,57,7,16,63,'2018-12-10 11:40:56','2018-12-10 11:42:02'),
(282,57,8,20,77,'2018-12-10 11:40:56','2018-12-10 11:42:08'),
(283,57,9,13,50,'2018-12-10 11:40:56','2018-12-10 11:42:16'),
(284,57,10,12,46,'2018-12-10 11:40:56','2018-12-10 11:42:27');

/*Table structure for table `trainees` */

DROP TABLE IF EXISTS `trainees`;

CREATE TABLE `trainees` (
  `trainee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) unsigned DEFAULT NULL,
  `trainor_id` int(10) unsigned NOT NULL,
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trainee_id`),
  KEY `trainor_id` (`trainor_id`),
  CONSTRAINT `trainor_id` FOREIGN KEY (`trainor_id`) REFERENCES `trainors` (`trainor_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `trainees` */

insert  into `trainees`(`trainee_id`,`dealer_id`,`trainor_id`,`fname`,`mname`,`lname`,`email`,`created_at`,`updated_at`) values 
(15,8,18,'Sofia Belle',NULL,'Tiburcio','sofia@gmail.com','2018-12-05 14:08:26','2018-12-05 14:08:26'),
(16,8,21,'Mariane Elabel',NULL,'Raymundo','mariane@gmail.com','2018-12-06 09:19:30','2018-12-06 09:19:30'),
(17,9,22,'Marc',NULL,'Roque','marc@gmail.com','2018-12-06 15:12:54','2018-12-07 09:44:53'),
(18,10,23,'Eman',NULL,'Magpantay','eman@gmail.com','2018-12-06 15:13:22','2018-12-07 09:29:55'),
(19,8,18,'Bricks',NULL,'Husky','bricks@gmail.com','2018-12-10 09:34:10','2018-12-10 09:34:10');

/*Table structure for table `training_histories` */

DROP TABLE IF EXISTS `training_histories`;

CREATE TABLE `training_histories` (
  `training_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(10) unsigned NOT NULL,
  `exam_schedule_id` int(10) unsigned DEFAULT NULL,
  `exam_detail_id` int(10) unsigned DEFAULT NULL,
  `trainee_exam_id` int(10) unsigned NOT NULL,
  `dealer_id` int(10) unsigned NOT NULL,
  `trainee_id` int(10) unsigned NOT NULL,
  `score` int(10) DEFAULT NULL,
  `result` enum('passed','failed') COLLATE utf8_unicode_ci DEFAULT 'failed',
  `date_taken` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`training_history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `training_histories` */

insert  into `training_histories`(`training_history_id`,`module_id`,`exam_schedule_id`,`exam_detail_id`,`trainee_exam_id`,`dealer_id`,`trainee_id`,`score`,`result`,`date_taken`,`created_at`,`updated_at`) values 
(50,1,38,39,49,8,15,5,'failed','2018-12-07 14:23:44','2018-12-07 14:26:20','2018-12-07 14:26:20'),
(51,1,38,39,50,8,16,6,'passed','2018-12-07 14:29:39','2018-12-07 14:31:56','2018-12-07 14:31:56'),
(52,1,39,42,51,8,16,6,'failed','2018-12-07 14:59:45','2018-12-07 15:01:41','2018-12-07 15:01:41'),
(53,1,40,45,53,8,15,4,'failed','2018-12-10 09:10:25','2018-12-10 09:14:32','2018-12-10 09:14:32'),
(54,1,40,45,54,8,16,2,'failed','2018-12-10 09:21:30','2018-12-10 09:23:09','2018-12-10 09:23:09'),
(55,1,40,45,55,8,19,8,'failed','2018-12-10 09:35:33','2018-12-10 09:37:35','2018-12-10 09:37:35'),
(56,1,40,46,56,9,17,9,'failed','2018-12-10 10:01:19','2018-12-10 10:03:10','2018-12-10 10:03:10'),
(57,1,40,47,57,10,18,9,'failed','2018-12-10 11:40:56','2018-12-10 11:42:29','2018-12-10 11:42:29');

/*Table structure for table `trainor_histories` */

DROP TABLE IF EXISTS `trainor_histories`;

CREATE TABLE `trainor_histories` (
  `trainor_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trainor_id` int(10) DEFAULT NULL,
  `module_detail_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trainor_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `trainor_histories` */

/*Table structure for table `trainors` */

DROP TABLE IF EXISTS `trainors`;

CREATE TABLE `trainors` (
  `trainor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) unsigned NOT NULL,
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trainor_id`),
  KEY `dealer_id` (`dealer_id`),
  CONSTRAINT `dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`dealer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `trainors` */

insert  into `trainors`(`trainor_id`,`dealer_id`,`fname`,`mname`,`lname`,`email`,`deleted_at`,`created_at`,`updated_at`) values 
(18,8,'Prince Ivan Kent',NULL,'Tiburcio','prince-tiburcio@gmail.com',NULL,'2018-10-25 13:19:10','2018-12-06 08:00:04'),
(21,8,'Charisse Jane',NULL,'Franco','charisse@gmail.com',NULL,'2018-12-06 08:01:24','2018-12-06 11:18:46'),
(22,9,'Mika',NULL,'Raymundo','mika@gmail.com',NULL,'2018-12-06 14:35:14','2018-12-06 14:35:14'),
(23,10,'Mycah',NULL,'Raymundo','mycah@gmail.com',NULL,'2018-12-06 14:35:32','2018-12-06 14:35:32');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('trainor','trainee') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_approved` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted_at` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `app_user_id_UNIQUE` (`app_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`user_id`,`app_user_id`,`name`,`email`,`password`,`user_type`,`remember_token`,`is_active`,`is_approved`,`deleted_at`,`created_at`,`updated_at`) values 
(32,'trainor_18','Tiburcio, Prince Ivan Kent Marquez','prince-tiburcio@gmail.com','$2y$10$bRFg4x7.CBjPiN2auuFveuksFVET5PSvhUn4lS60FfcH4.pJExzU6','trainor','G5iGVATUlZIela0bcJsThDNjjCKPZTn3iFfhNLPyFPbeivfrZrP79BIToZPf',0,1,NULL,'2018-10-25 13:19:10','2018-12-10 09:34:18'),
(34,'trainee_15','Tiburcio, Sofia Belle ','sofia@gmail.com','$2y$10$Ql9ptmKLArTHGJaygza19e5UDK/oJwYRq8jf1yhfvDKeRj8cJS4rK','trainee','hG52QJt5R6s8gMBe2J2rsc8VOsyjPVfGFxD2Pl8Ook2O8ni0XHuGr80p5JT0',0,1,NULL,'2018-11-29 15:48:51','2018-12-10 09:21:17'),
(39,'trainor_21','Franco, Charisse Jane ','charisse@gmail.com','$2y$10$aYTGAc31do7YOREzoEtzN.RA0vk5ufcGyeYfilA.50XVRZ6eBiYCW','trainor','Q9OfHOCT69p6P8pzg2lJp5uMqvIkmwj078YfuYxUvmSd1uw1qBSqzbP4M7uA',0,1,NULL,'2018-12-06 08:01:24','2018-12-07 09:48:29'),
(40,'trainee_16','Raymundo, Mariane Elabel ','mariane@gmail.com','$2y$10$dIdne4AEpGUahjYz7zISROC9RbUB02VkMKSti3ArHXU0mphW/BOea','trainee','3pIdYOeY8oyKtIhPFdo9rpLMcruQ1HrQW57DYMT3pCUuPcP7qdABdyQysH6M',0,1,NULL,'2018-12-06 09:19:30','2018-12-10 09:32:39'),
(41,'trainor_22','Raymundo, Mika ','mika@gmail.com','$2y$10$QI/hxHmZfuCiFgrRLo3oB.IAgEW2GDc/ctvq84z5wkbtlAjc0QhjG','trainor','F1srAILJaflh8wSqUXeSwWPFFK7AsSKe2oshUfd68z2b1So8j9l7beenf4Oo',0,1,NULL,'2018-12-06 14:35:14','2018-12-10 10:01:11'),
(42,'trainor_23','Raymundo, Mycah ','mycah@gmail.com','$2y$10$oZSi.l66DqChzo6/KSZ15eNBz4uVcdFH1P2u5xPp0e6wFGxbFv1lW','trainor','oYrpNHlUCZFWPlQDigYoasUSp5KoK7sg23RIGLnqbQZakRXlkGjx3Bx2kZxR',0,1,NULL,'2018-12-06 14:35:32','2018-12-06 15:15:16'),
(43,'trainee_17','Roque, Marc ','marc@gmail.com','$2y$10$d1UwZdqKYfINmuphhpX5ce.26x7WnuRiSKIed.KpXu8JBXPHvIWyK','trainee','pXdwPEdklH4ZvcMLy5THi8js1BK6eHT2S9Fvp9ufdJAc6rDZW3J7xDxGS0EC',0,1,NULL,'2018-12-06 15:12:54','2018-12-10 11:40:41'),
(44,'trainee_18','Magpantay, Eman ','eman@gmail.com','$2y$10$dk1InOHtBvXygCYKjcfobuifOtIMDH3FXUImcUqBsgcPTVlWUEBmi','trainee','7l4pOvsxFTRpX8w9okiH4ZVaNiSBFk0pNhcQrFxD0I9cTk88fSN6qjXQYHCT',1,1,NULL,'2018-12-06 15:13:22','2018-12-10 11:40:53'),
(45,'trainee_19','Husky, Bricks ','bricks@gmail.com','$2y$10$zeYGKdWfjdvrwLG2gTtiduqlyNC.QUPKsgBuF0/K1FjH1WjzqCVxe','trainee','xf2pM4LeqzMdiA2I2oRAMKMv2Ek8gOwBLjDkkGm0ACI2Y8DW3omXhCGT2wWm',0,1,NULL,'2018-12-10 09:34:10','2018-12-10 10:00:53');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
